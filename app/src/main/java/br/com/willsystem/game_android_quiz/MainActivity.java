package br.com.willsystem.game_android_quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import br.com.willsystem.game_android_quiz.view.Helpers;

public class MainActivity extends AppCompatActivity {

private RadioGroup rdGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    Helpers a = new Helpers();
    public void layoutQuestoes(View view){
        setContentView(R.layout.layout_questoes);


        RadioGroup rgrpMapType = (RadioGroup) findViewById(R.id.rdgrp);
        int selectedTypeId = rgrpMapType.getCheckedRadioButtonId();
        //RadioButton rbMapType = (RadioButton) findViewById(selectedTypeId);
        //if (rbMapType != null) // This will be null if none of the radio buttons are selected
        //    rgrpMapType.clearCheck();
    }

    public boolean verificaRadio(){
        RadioButton rb1 = (RadioButton)findViewById(R.id.radioOpc1);
        RadioButton rb2 = (RadioButton)findViewById(R.id.radioOpc2);
        RadioButton rb3 = (RadioButton)findViewById(R.id.radioOpc3);
        RadioButton rb4 = (RadioButton)findViewById(R.id.radioOpc4);
        if (!rb1.isChecked() && !rb2.isChecked() && !rb3.isChecked() && !rb4.isChecked())
            return false;
        else return true;
    }
}