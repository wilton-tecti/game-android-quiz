package br.com.willsystem.game_android_quiz.view;

import android.app.AlertDialog;

/**
 * Created by Vinicius on 30/08/2016.
 */
public class Helpers {
    private AlertDialog alerta;
    public void alerta(){
        AlertDialog.Builder builder =  new AlertDialog.Builder(null);
        builder.setTitle("Atenção");
        builder.setMessage("É necessário informar alguma opção!");
        alerta = builder.create();
        alerta.show();
    }

}
