package br.com.willsystem.game_android_quiz.model;

/**
 * Created by will on 23/08/16.
 */
public class Livro {

    private String nome;
    private String autor;
    private String editora;
    private String paginares;

    public Livro() {
    }

    public Livro(String nome, String autor, String editora, String paginares) {
        this.nome = nome;
        this.autor = autor;
        this.editora = editora;
        this.paginares = paginares;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getPaginares() {
        return paginares;
    }

    public void setPaginares(String paginares) {
        this.paginares = paginares;
    }
}
