package br.com.willsystem.game_android_quiz.model;

/**
 * Created by will on 30/08/16.
 */
public class Partida {

    private String Jogador;
    private Integer pontototal;

    public Partida() {
    }

    public String getJogador() {
        return Jogador;
    }

    public void setJogador(String jogador) {
        Jogador = jogador;
    }

    public Integer getPontototal() {
        return pontototal;
    }

    public void setPontototal(Integer pontototal) {
        this.pontototal = pontototal;
    }
}
