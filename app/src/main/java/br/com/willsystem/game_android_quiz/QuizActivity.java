package br.com.willsystem.game_android_quiz;

import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

import br.com.willsystem.game_android_quiz.model.Pergunta;

public class QuizActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        GridView gridview = (GridView) findViewById(R.id.gridView);

        List<Pergunta> lista = new ArrayList<>();



        gridview.setAdapter(new PerguntaAdapter(this,lista));

    }
}
